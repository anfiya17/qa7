/**
 * Created by IT-school on 07.02.2018.
 */
public abstract class User implements IUser {

    private String name;
    private int age;
    private boolean male;

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "name -" + name;
    }

    abstract void print();




    }

