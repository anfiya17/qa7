/**
 * Created by IT-school on 07.02.2018.
 */
public class App {
    public static void main(String[] args){
        User user = new StudentsUser("ds",22,true);
        //to create new Object User
        System.out.println(user);
        user.setAge(18);
        System.out.println(user.getAge());
        user.setName("Sergey");
        System.out.println(user.getName());

        StudentsUser studentsUser = new StudentsUser("Jack", 34, true);
        studentsUser.setAge(34);
        System.out.println(studentsUser.getAge());

        studentsUser.generateId();
        System.out.println(studentsUser.getId());
    }
}
